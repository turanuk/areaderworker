var azure = require('azure');

var tableService = azure.createTableService();
var queueService = azure.createQueueService();
var feedDataTable = 'FeedDataTable';
var feedMetaDataTable = 'FeedMetaDataTable';
var feedsToQueryQueue = 'feedstoqueryqueue';
var quickQueue = 'feedstoqueryquicklyqueue';
var loggingMessageQueue = 'loggingmessagesqueue';

var createQueue = function (queueName) {
	queueService.createQueue(queueName, function (err) {
		console.log(queueName + ' created.');
	});
}

var createTable = function (tableName) {
	tableService.createTable(tableName, function (err) {
		console.log(tableName + ' created.');
	});
}

createTable(feedDataTable);
createTable(feedMetaDataTable);

createQueue(feedsToQueryQueue);
createQueue(quickQueue);
createQueue(loggingMessageQueue);