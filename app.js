var express = require('express'),
    htmlparser = require('htmlparser'),
    request = require('request'),
    azure = require('azure'),
    helpers = require('./helpers.js'),
    azureutil = require('azure/lib/util/util');

var app = module.exports = express.createServer();
var tableService = azure.createTableService();
var queueService = azure.createQueueService();
var tableName = 'areader';

var feedDataTable = 'FeedDataTable';
var feedMetaDataTable = 'FeedMetaDataTable';
var feedsToQueryQueue = 'feedstoqueryqueue';

//Configuration
app.configure(function () {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.logger());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function () {
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function () {
  app.use(express.errorHandler());
});

//Routes
app.get('/', function (req, res) {
  res.render('index.ejs', { locals: {
    title: 'Welcome'
    }
  });
});

//DEBUG ONLY: Deletes the table
app.get('/DeleteTable', function (req, res) {
  tableService.deleteTable(feedDataTable, null, function (err) {
    res.render('index.ejs', { locals: {
      title: 'Deleted Table'
      }
    });
  });
});

//DEBUG ONLY: Clears the queue
app.get('/ClearQueue', function (req, res) {
  queueService.clearMessages(feedsToQueryQueue, null, function (err) {
    res.render('index.ejs', { locals: {
      title: 'Cleared Queue'
      }
    });
  });
});

//Displays feed prompting for user input to add a feed to the queue
app.get('/AddFeedToQueue', function (req, res) {
  res.render('addfeed', { locals: {
    title: 'Add Feed'
    }
  });
});

//Server side logic handling the actual addition of a feed to the queue
app.post('/AddFeedToQueue', function (req, res) {
  var addFeedToQueue = function () {
    queueService.createMessage(feedsToQueryQueue, req.body.feedUrl, function (err) {
      if (!err) {
        res.redirect('/ViewQueue');
      } else {
        helpers.renderError(res);
      }
    });
  }
  helpers.createQueue(queueService, feedsToQueryQueue, res, addFeedToQueue);
});

//Displays the item(s) in the queue
app.get('/ViewQueue', function (req, res) {
  var getMessages = function() {
    var args = {
      numofmessages: 10
    };
    queueService.peekMessages(feedsToQueryQueue, args, function (err, items) {
      if (!err) {
        res.render('feeditems.ejs', { locals: {
          title: 'List of Items',
          serverItems: items
          }
        });
      } else {
        helpers.renderError(res);
      }
    });
  }
  helpers.createQueue(queueService, feedsToQueryQueue, res, getMessages);
});

//Displays the table that contains all the feed information
app.get('/ViewTable', function (req, res) {
  var getEntries = function () {
    var query = azure.TableQuery
      .select()
      .from(feedDataTable);
    tableService.queryEntities(query, function (err, results) {
      if (!err) {
        res.render('viewfeed.ejs', { locals: {
          title: 'View Feed',
          tableEntries: results
          }
        });
      } else {
        helpers.renderError(res);
      }
    });
  }
  helpers.createTable(tableService, feedDataTable, res, getEntries);
});


app.listen(1337);
console.log('Server active on port %d', app.address().port);