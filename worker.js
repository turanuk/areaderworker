//This is the worker role for aReader

var request = require('request')
    feedme = require('feedme'),
    azure = require('azure'),
    helpers = require('./helpers.js'),
    ArrayStream = require('arraystream'),
    async = require('async');

var tableService = azure.createTableService();
var mainQueueService = azure.createQueueService();
var quickQueueService = azure.createQueueService();
var loggingQueueService = azure.createQueueService();
var feedsToQueryQueue = 'feedstoqueryqueue';
var quickQueue = 'feedstoqueryquicklyqueue';
var loggingMessageQueue = 'loggingmessagesqueue';

///UTILITY FUNCTIONS
var sanitizeLink = function (item) {
  var itemLink = item.link;
  //If there is an href property, use that
  if (itemLink.href) { return itemLink.href; }
  //If there is a FeedBurner specific property for the original link, use that
  if (item['feedburner:origlink']) { return item['feedburner:origlink']; }
  if (helpers.isArray(itemLink)) { 
    //Look for the alternate link in this array
    for (var i = 0; i < itemLink.length; i++) {
      if (itemLink[i].rel && itemLink[i].href && (itemLink[i].rel === 'alternate' || itemLink[i].rel === 'alt')) {
        return itemLink[i].href;
      }
    }
    //Couldn't find alternate link. If the array has an href element then, return the first one
    if (itemLink[0].href) {
      return itemLink[0].href;
    } 
    //Must be some kind of string type if we can't fit it into these molds
    return itemLink[0];
  }
  //Sanitization for known cases passed, assume this is accurage and move on to return original
  return itemLink;
}

var sanitizePubDate = function (item) {
  if (item.pubdate) { return new Date(item.pubdate); }
  if (item.updated) { return new Date(item.updated); }
  return new Date();
}

var sanitizeDescription = function (item) {
  if (item.description) { return item.description; }
  if (item.summary) { return item.summary; }
  if (item.content) { return item.content; }
  return 'No description found';
}

//MAIN CODE FLOW
var startEventLoop = function () { 
  async.waterfall([
    //Query quick queue first
    function (callback) {
      quickQueueService.getMessages(quickQueue, function(err, message) {
        if(!err) {
          if(message[0]) {
            callback(null, message[0], quickQueue, quickQueueService);
          } else {
            callback(null, null, null, null);
          }
        } else {
          helpers.logError(loggingQueueService, loggingMessageQueue, err, 'Failed to get message from quick query queue');
        }
      });
    },

    //Query main queue next
    function (message, queueName, inputQueueService, callback) {
      //If there is a value from the quick queue, pass through to the next waterfall
      if (message) {
        callback(null, message, queueName, inputQueueService)
      }
      mainQueueService.getMessages(feedsToQueryQueue, function(err, message) {
        if(!err) {
          if(message[0]) {
            callback(null, message[0], feedsToQueryQueue, mainQueueService);
          } else {
            //Nothing found in queue, break execution
            callback('Empty Queue');
          }
        } else {
          callback(err, 'Failed to get message from queue');
        }
      });
    },

    //Delete the message from the queue 
    function (message, queueName, inputQueueService, callback) {
      inputQueueService.deleteMessage(queueName, message.messageid, message.popreceipt, function (err) {
        if (!err) {
          var messagetext = message.messagetext.toLowerCase();
          if (messagetext.indexOf('http://') == -1) {
            callback('Invalid protocol for feed URL');
          } else {
            callback(null, messagetext);
          }
        } else {
          callback(err, 'Deleting message from queue failed');
        }
      });
    },

    //Parse feed and update feed title in database
    function(feedUrl, callback) {
      request({ uri: feedUrl }, function(err, response, body) {
          if(!err) {
              //Execution Logic
              feedparser = new feedme();
              var items = new Array();
              var stream = new ArrayStream([body]);
              stream.pipe(feedparser);
              //Feedparser Events
              feedparser.on('item', function(feedItem) {
                  items.push(feedItem);
              });
              feedparser.on('title', function(domTitle) {
                request(
                  {
                    method: 'POST',
                    uri: 'http://127.0.0.2:4242/api/Feed/UpdateFeed',
                    json: { 
                      FeedUrl: feedUrl,
                      Title: domTitle
                    }
                  },
                  function(err) {
                    if(err) {
                      callback(err);
                    } else {
                      //Optional log message
                      helpers.logMessage(loggingQueueService, loggingMessageQueue, 'Updated MetaData for Feed');
                    }
                  }
                );
              });
              feedparser.on('error', function(err) {
                  callback(err, 'Error parsing ' + feedUrl);
              });
              feedparser.on('end', function() {
                  callback(null, items, feedUrl)
              });
          } else {
              callback(err);
          }
      });
    },

    //Put feed items into database
    function (items, feedUrl, callback) {
      var counter = 0;
      var feedDataTableEntries = new Array();
      //Populate array
      for (var i = 0; i < items.length; i++) {
        //Sanitize Link
        var itemLink = sanitizeLink(items[i]);
        //Sanitize Publication Date
        var publicationDate = sanitizePubDate(items[i]);
        //Sanitize Description
        var itemDescription = sanitizeDescription(items[i]);
        //Create Table Entry Object
        var tableEntry = {
            FeedUrl: feedUrl,
            FeedItemLink: itemLink,
            Title: items[i].title,
            Description: itemDescription,
            PublicationDate: publicationDate
        };
        feedDataTableEntries.push(tableEntry);
      } 
      request(
        {
          method: 'POST',
          uri: 'http://127.0.0.2:4242/api/Feed/AddFeedItems',
          json: { 
              feedItemsToAdd: feedDataTableEntries
          }
        },
        function(err) {
          if (err) {
            callback(err, 'Problem Updating');
          } else {
            helpers.logMessage(loggingQueueService, loggingMessageQueue, 'Updated Feed');
            callback(null);
          }
        }
      );
    }
  ],

    //Final callback/Exit Point
    function(err, results) {
      //If this is an empty queue, no operation is required
      if (err !== 'Empty Queue') {
        helpers.logError(loggingQueueService, loggingMessageQueue, err, results);
      }
    }
  );
}

//MAIN EVENT LOOP
try {
    setInterval(startEventLoop, 200)
} catch (e) {
    helpers.logError(loggingQueueService, loggingMessageQueue, err, 'FeedDataWorkerRole JavaScript Failure');
}