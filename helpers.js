//Helper functions
var exports = module.exports;

exports.isArray = function (input) {
  return Object.prototype.toString.call(input) === '[object Array]';
}

exports.logMessage = function (queueService, queueName, message) {
  var timeStamp = new Date();
  if (message) {
    var messageText = '[ ' + timeStamp + ' ] ' + message;
    //Live
    //queueService.createMessage(queueName, messageText, function() { });
    //Local
    console.log(messageText);
  }
}

exports.logError = function (queueService, queueName, err, message) {
  var timeStamp = new Date();
  var messageText = '';
  if (err && err.message && message) {
    messageText = '[[[ ERROR ]]] ' + timeStamp + message + ': ' + err.message;
  } else if (err) {
    messageText = '[[[ ERROR ]]] ' + timeStamp + err;
  }

  //Live
  //queueService.createMessage(queueName, messageText, function() { });
  //Local
  console.log(messageText);
}